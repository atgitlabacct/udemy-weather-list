import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Sparklines,
  SparklinesLine,
  SparklinesReferenceLine,
} from 'react-sparklines';
import GoogleMap from '../components/google_map';

const average = data => {
  return _.round(_.sum(data) / data.length);
};

const Chart = props => {
  return (
    <div>
      <Sparklines height={props.height} width={props.width} data={props.data}>
        <SparklinesLine color={props.color} />
        <SparklinesReferenceLine type="avg" />
      </Sparklines>
      <div>
        {average(props.data)} {props.units}
      </div>
    </div>
  );
};

export class WeatherList extends Component {
  renderWeather(cityData) {
    const city = cityData.city;
    const temps = cityData.list.map(weather => weather.main.temp);
    const pressures = cityData.list.map(weather => weather.main.pressure);
    const humidities = cityData.list.map(weather => weather.main.humidity);
    const {lon, lat} = cityData.city.coord;

    return (
      <tr key={city.id}>
        <td>
          <GoogleMap lon={lon} lat={lat} />
        </td>
        <td>
          <Chart
            units="K"
            height={120}
            width={150}
            data={temps}
            color="orange"
          />
        </td>
        <td>
          <Chart
            units="hPa"
            height={120}
            width={150}
            data={pressures}
            color="red"
          />
        </td>
        <td>
          <Chart
            units="%"
            height={120}
            width={150}
            data={humidities}
            color="red"
          />
        </td>
      </tr>
    );
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (K)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>{this.props.weather.map(this.renderWeather)}</tbody>
      </table>
    );
  }
}

function mapStateToProps({weather}) {
  return {weather};
}

export default connect(mapStateToProps)(WeatherList);
