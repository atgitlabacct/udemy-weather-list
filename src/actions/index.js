import axios from 'axios';

const API_KEY = 'e972f012e3ca9586d72d23680b25eaba';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

const buildUrl = query => {
  return `${ROOT_URL}&q=${query},us`;
};

export function fetchWeather(city) {
  const request = axios.get(buildUrl(city));

  return {
    type: FETCH_WEATHER,
    payload: request // Return the promise
  };
}
